import React from 'react';
import {
  Switch,
  Route,
  BrowserRouter
} from 'react-router-dom';

import Game from './pages/Game/game';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={Game} />
      </Switch>
    </BrowserRouter>
  );
};