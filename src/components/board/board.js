import React, { useState } from 'react'; // Packages

// styles
import './board.css';

// components
import Square from '../squares/squares';

export default function Board() {
  const [squares, setSquares] = useState(Array(9).fill(null)); // Lista com todos os quadrados existentes
  const [xIsNext, setNext] = useState(true); // X é o próximo a jogar?
  const [scoreOne, setScoreOne] = useState(0); // Pontuação do jogador 1
  const [scoreTwo, setScoreTwo] = useState(0); // Pontuação do jogador 2
  const [nextPlayer, setNextPlayer] = useState(0); // Próximo a jogar
  const [winner, setWinner] = useState(false); // Existe vencedor (true) : Não existe vencedor (false)
  const [round, setRound] = useState(0); // Número de rodadas
  const [status, setStatus] = useState("Next player: "); // Status da partida - "Próximo a jogar" : "Ganhador" : "Empate"

  // Função responsável por mudar quem jogará 
  async function handleNextPlayer() {
    if( nextPlayer === 0 ) { // O próximo a jogar é o jogador 0?
      setNextPlayer(1);
      setNext(!xIsNext);
    } else { // Caso contrário 
      setNextPlayer(0);
      setNext(!xIsNext);
    }
  };

  // Função responsável por atualizar o round, ou seja, quantas rodadas se passaram
  async function updateRound() {
    await setRound(preventValue => preventValue + 1);
  }; 

  // Função responsável por verificar se algum jogador ganhou
  function checkVictory(squares, position) {
    const possibilities = {
      '0': [[0, 1, 2], [0, 4, 8], [0, 3, 6]],
      '1': [[0, 1, 2], [1, 4, 7]],
      '2': [[0, 1, 2], [6, 4, 2], [2, 5, 8]],
      '3': [[3, 4, 5], [0, 3, 6]],
      '4': [[3, 4, 5], [0, 4, 8], [2, 4, 6], [1, 4, 7]],
      '5': [[3, 4, 5], [2, 5, 8]],
      '6': [[6, 7, 8], [2, 4, 6], [0, 3, 6]],
      '7': [[6, 7, 8], [1, 4, 7]],
      '8': [[6, 7, 8], [0, 4, 8], [2, 5, 8]],
    }; // Todas as possibilidades de ganhar para cada jogada
    let filter_possibilities = possibilities[[position]]; // Possibilidades filtradas de acordo com a jogada 
    
    for( let pos = 0; pos < filter_possibilities.length; pos++ ) { // Percorrendo todas as possibilidades que foram filtradas
      const [a, b, c] = filter_possibilities[pos]; // Pegando as três posições de cada possibilidade

      if( squares[a] === squares[b] && squares[a] === squares[c] && squares[a] !== null ) { // Verificando se "A" é igual "B" e "A" é igual a "C"
        setWinner(true); // Definindo que existe um ganhador

        return squares[a]; // Retornando o ganhador
      };
    };
    
    return null; // Não existe ganhador, então, retornar nulo
  };

  // Função principal, responsável por mudar os valores de cada quadrado e rodar o jogo
  function handleClick(position) {
    if( squares[position] === null && winner === false ) { // O valor desse quadrado é nulo e não existe vencedor?
      updateRound(); 
      
      const square_list = squares.slice();
      square_list[position] = xIsNext ? 'X': 'O'; // Se X é igual a true, o quadrado de uma determinada posição receberá "X", caso contrário, recebe "0"
      setSquares(square_list);

      if( round >= 4 ) { // Testando se o round é maior ou igual a 4 (EXPLICAR O MOTIVO DE SER 4 E NÃO 5)
        let win = checkVictory(square_list, position);

        if( win !== null ) { // Se existir vencedor
          setStatus("Winner: ");

          if( win === 'X' ) { // Se o vencedor é "X"
            setScoreOne(scoreOne + 1);
          } else { // Caso contrário
            setScoreTwo(scoreTwo + 1);
          };

          return;
        }
      };
      
      if( round >= 8 && winner === false) { // A quantidade de rounds é maior ou igual a 8 e não existe vencedor
        setStatus("Draw!")
        setWinner(true);

        return;
      };
      
      handleNextPlayer();
    };
  };

  // Funcionalidade que permite jogar novamente, ou seja, reiniciar o jogo mantendo a quantidade de vitórias de cada jogador
  function playAgain() {
    setSquares(Array(9).fill(null));
    setWinner(false);
    setStatus("Next player: ");
    setRound(0);
    handleNextPlayer();
  };

  // Funcionalidade que renderiza o botão de jogar novamente se existir um vencedor ou se tiver um empate
  function setButton() {
    if( winner ) {
      return (
        <button className="play-again"
          onClick={() => playAgain()}
        >JOGAR NOVAMENTE</button>
      );
    }
  };

  return (
    <>
    <div className="game">
      <div className="status">{status}&nbsp;<div className={xIsNext ? 'X' : 'O'}>{xIsNext ? 'X' : 'O'}</div></div>
      <div className="score"><div className="X">P1: {scoreOne}</div>&nbsp;-&nbsp;<div className="O">P2: {scoreTwo}</div></div>
      <div className="board-row">
        <Square value={squares[0]} onClick={() => handleClick(0)}/>
        <Square value={squares[1]} onClick={() => handleClick(1)}/>
        <Square value={squares[2]} onClick={() => handleClick(2)}/>
      </div>
      <div className="board-row">
        <Square value={squares[3]} onClick={() => handleClick(3)}/>
        <Square value={squares[4]} onClick={() => handleClick(4)}/>
        <Square value={squares[5]} onClick={() => handleClick(5)}/>
      </div>
      <div className="board-row">
        <Square value={squares[6]}  onClick={() => handleClick(6)}/>
        <Square value={squares[7]}  onClick={() => handleClick(7)}/>
        <Square value={squares[8]}  onClick={() => handleClick(8)}/>
      </div>
      <div className="btn-container">
        {setButton()}
      </div>
    </div>
    </>
  );
}