import React from 'react';

import './squares.css';

export default function Square(props) {
  return (
    <>
    <button className={`square ${props.value}`} onClick={() => props.onClick()}>
      {props.value}
    </button>
    </>
  )
};