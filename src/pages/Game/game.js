import React from 'react'; // packages

// styles
import './game.css'
// components
import Board from '../../components/board/board';

export default function Game() {
  return (
    <div className="game" >
      <div className="game-board">
        <Board className="board" />
      </div>
    </div>
  )
};